# Agent-based model using Polars

[![pipeline status](https://gitlab.unimelb.edu.au/rgmoss/polars-agent-model/badges/master/pipeline.svg)](https://gitlab.unimelb.edu.au/rgmoss/polars-agent-model/-/commits/master)
[![coverage report](https://gitlab.unimelb.edu.au/rgmoss/polars-agent-model/badges/master/coverage.svg)](https://gitlab.unimelb.edu.au/rgmoss/polars-agent-model/-/commits/master)

This is a proof-of-concept agent-based model built on top of [Polars](https://pola.rs/).
The model population is represented as a single data frame, where each row represents an agent.

See the [online documentation](https://rgmoss.pages.gitlab.unimelb.edu.au/polars-agent-model/) for details.

Note that this repository uses the [pre-commit package](https://pre-commit.com/) to perform various checks before allowing the commit to be created.
We can install `pre-commit` with [pipx](https://pipx.pypa.io/stable/), and then install the `pre-commit` hook, with the following commands:

```sh
pipx install pre-commit
pre-commit install
```

We also use the [nox automation tool](https://nox.thea.codes/en/stable/) to run test cases (with [pytest](https://docs.pytest.org/)) and to format and lint code (with [ruff](https://docs.astral.sh/ruff/)).
We can install `nox` with `pipx`:

```sh
pipx install nox
```
