import nox
from pathlib import Path


@nox.session()
def tests(session):
    """Run test cases and record the test coverage."""
    session.install('.[tests]')
    # Run the test cases and report the test coverage.
    package = 'polabm'
    session.run(
        'python3',
        '-bb',
        Path(session.bin) / 'pytest',
        f'--cov={package}',
        '--pyargs',
        package,
        './tests',
        './doc',
        *session.posargs,
    )
    # Ensure that regression test outputs have not changed.
    session.run(
        'git', 'diff', '--exit-code', '--stat', 'tests/', external=True
    )


@nox.session(reuse_venv=True)
def ruff(session):
    """Check code for linter warnings and formatting issues."""
    check_files = ['src', 'tests', 'doc', 'noxfile.py']
    session.install('ruff ~= 0.4')
    session.run('ruff', 'check', *check_files)
    session.run('ruff', 'format', '--diff', *check_files)


@nox.session(reuse_venv=True)
def docs(session):
    """Build the HTML documentation."""
    session.install('.')
    session.install('-r', 'requirements-docs.txt')
    session.run(
        'sphinx-build', '-W', '-b', 'html', './doc', './doc/build/html'
    )


@nox.session(reuse_venv=True)
def mypy(session):
    """Check code for typing issues."""
    session.install('.')
    session.install('mypy')
    session.run('mypy', 'src')
