#!/bin/bash
#
# As of Polars 0.19.6, the polars package requires AVX and FMA CPU features:
#
# https://github.com/pola-rs/polars/blob/py-0.19.6/.github/workflows/release-python.yml#L77
#
# If these features are not present, we must use the polars-lts-cpu package:
#
# https://github.com/pola-rs/polars/issues/5999
#

if grep '^flags' /proc/cpuinfo | grep '\<avx\>' | grep -q '\<fma\>'; then
    echo "AVX and FMA are supported"
else
    echo "AVX and FMA are not supported"
    echo "Changing dependency from polars to polars-lts-cpu ..."
    sed -i 's/"polars\[/"polars-lts-cpu\[/' pyproject.toml
fi

# NOTE: also print the amount of free RAM, to identify runners with low RAM.
awk '/MemFree/ { printf "Free RAM: %.3f GB\n", $2/1024/1024 }' /proc/meminfo
