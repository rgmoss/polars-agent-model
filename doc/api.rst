API documentation
=================

.. py:module:: polabm

Populations
-----------

.. autoclass:: Population
   :members:

Components
----------

.. autoclass:: Component
   :members:

.. autoclass:: Age
   :members:

Actions
-------

.. autoclass:: EagerAction
   :members:

.. autoclass:: LazyAction
   :members:

.. autoclass:: RenameFinaliser
   :members:

.. autofunction:: apply_actions
