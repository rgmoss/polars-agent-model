project = 'polabm'
copyright = '2024, Rob Moss'
version = '0.1'
release = '0.1.0'

source_suffix = '.rst'
master_doc = 'index'
language = 'en'

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.mathjax',
    'sphinx_togglebutton',
]

# Link to relevant documentation for other packages.
#
# See https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html
# for documentation and examples.
intersphinx_mapping = {
    'numpy': ('https://numpy.org/doc/stable/', None),
    'polars': ('https://docs.pola.rs/py-polars/html/', None),
}
intersphinx_disabled_reftypes = ['*']

html_theme = 'pydata_sphinx_theme'
html_show_sourcelink = False
html_context = {
    'gitlab_url': 'https://gitlab.unimelb.edu.au',
    'gitlab_user': 'rgmoss',
    'gitlab_repo': 'polars-agent-model',
    'gitlab_version': 'master',
    'doc_path': 'doc',
}
html_theme_options = {
    'logo': {
        'text': 'polabm',
    },
    'show_toc_level': 2,
    'show_nav_level': 2,
    'navigation_depth': 2,
    'navigation_with_keys': False,
    # Left-align navigation links in the page header.
    'navbar_align': 'left',
    # Move `sphinx-version` from start to center to shrink the footer.
    'footer_start': ['copyright'],
    'footer_center': ['sphinx-version'],
    'use_edit_page_button': True,
    'header_links_before_dropdown': 6,
    'icon_links_label': 'Quick Links',
    'icon_links': [
        {
            'name': 'GitLab',
            'url': 'https://gitlab.unimelb.edu.au/rgmoss/polars-agent-model',
            'icon': 'fa-brands fa-square-gitlab',
            'type': 'fontawesome',
        },
    ],
}
