Using Polars
============

`Polars <https://pola.rs/>`__ is a high performance data frame library that automatically distributes data frame operations across available CPU cores.
See the `Polars user guide <https://docs.pola.rs/>`__ and the `Python API reference <https://docs.pola.rs/py-polars/html/reference/>`__ for details.

.. tip::

   Where possible, use the `Lazy API <https://docs.pola.rs/user-guide/lazy/>`__ and the :external:std:doc:`reference/lazyframe/index` class, rather than the :external:std:doc:`reference/dataframe/index` class.
   This allows for whole-query optimisation in addition to parallelism, and is the preferred (and highest-performance) mode of operation for polars.

Useful functions and methods
----------------------------

Potentially useful Polars features include:

- Using :external:meth:`~polars.DataFrame.pipe` to apply user-defined functions within a series of operations.

- Using :external:meth:`~polars.DataFrame.to_struct` to group multiple columns into a single :external:class:`~polars.datatypes.Struct` column, and :external:meth:`~polars.DataFrame.unnest` to unpack a :external:class:`~polars.datatypes.Struct` column into the original columns.

- Using :external:func:`polars.when` to modify only a subset of rows, or to apply different operations to different subsets of rows.

Non-determinism
---------------

Because Polars can distribute data frame operations over multiple cores, you may not obtain identical results each time you run a simulation.

- Pass a known seed to :external:meth:`~polars.DataFrame.sample`.

- Pass ``maintain_order=True`` to methods such as :external:meth:`~polars.DataFrame.group_by` and :external:meth:`~polars.DataFrame.unique`.

- Use :external:meth:`~polars.DataFrame.sort` as a final operation to ensure a consistent row ordering.

Other notes
-----------

- Use the ``"outer_coalesce"`` strategy for :external:meth:`outer joins <polars.DataFrame.join>`, to avoid including duplicate key columns from both data frames.
