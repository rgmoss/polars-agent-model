Agent-based model using Polars
==============================

This is a proof-of-concept agent-based model built on top of `Polars <https://pola.rs/>`__.

The model is structured according to the following principles:

- The model population is represented as a single data frame, where each row represents an individual.

- Within this population, we will want to simulate processes such as ageing, mobility, infection, recovery, acquisition of immunity, and waning of immunity.

  Each process should be implemented as an independent action that potentially modifies the population data frame, based on the data frame's contents at the start of the time-step.

- At each time-step, we want to apply all of these actions, and then update the population data frame.

  I have chosen to implement this as a sequence of "actions", followed by a sequence of "finalisers", where:

  - Actions add new (temporary) columns to the population data frame, to represent quantities such as updated state variables; and

  - Finalisers update the model state columns and remove the temporary columns.

  As an example, the :class:`~polabm.Age` class adds the following effects to a population model:

  1. It creates the column (``"age"``) to record each individual's age (measured in time-steps);

  2. It adds an action that calculates the new ages at each time-step and stores these values in the ``"age_updated"`` column; and

  3. It adds a finaliser that replaces the ``"age"`` column with the values in the ``"age_updated"`` column.

  Other actions can access the ``"age"`` column during each time-step, and will obtain each individual's age at the start of the time-step.

We can then define "observers" that record outputs of interest (line-listed data, summary statistics, etc).
I suspect we will want to record observations:

1. Before/after each time-step; and

2. Between the actions and finalisers at each time-step.

The second option will allow us to directly observe events (e.g., new infections, loss of immunity) without having to compare agent current states to their previous states.

.. toctree::
   :hidden:
   :maxdepth: 2

   development
   polars
   api
