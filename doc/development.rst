Development
===========

We use the `pre-commit package <https://pre-commit.com/>`__ to perform various checks before allowing commits to be created.
We can install ``pre-commit`` with `pipx <https://pipx.pypa.io/stable/>`__, and then install the ``pre-commit`` hook in the local repository, with the following commands:

.. code-block:: sh

   pipx install pre-commit
   pre-commit install

.. admonition:: The pre-commit hooks (``.pre-commit-config.yaml``).
   :class: dropdown seealso

   .. literalinclude:: ../.pre-commit-config.yaml

In the pre-commit hook we run the `ruff <https://docs.astral.sh/ruff/>`__ linter and code formatter.
It can be installed with ``pipx``:

.. code-block:: sh

   pipx install ruff

We also use the `nox automation tool <https://nox.thea.codes/en/stable/>`__ to run test cases with `pytest <https://docs.pytest.org/>`__, and to format and lint code with `ruff <https://docs.astral.sh/ruff/>`__.
We can install ``nox`` with ``pipx``:

.. code-block:: sh

   pipx install nox

You can list the available ``nox`` sessions by running ``nox -l``.
For example:

``nox -s ruff``
   Check the code for linter warnings and formatting issues.

``nox -s tests``
   Run test cases and report the test coverage.

``nox -s docs``
   Build the package documentation (which you are currently reading).

``nox -s mypy``
   Check the code for typing issues.

.. admonition:: The nox session definitions (``noxfile.py``).
   :class: dropdown seealso

   .. literalinclude:: ../noxfile.py
