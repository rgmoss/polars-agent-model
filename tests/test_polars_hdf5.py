"""
We may wish to collect all output tables for a set of simulations into a
single output file, in which case HDF5 is probably the best choice.

The one complication is that string fields should be stored as UTF-8 encoded
bytes, and we need to manually decode these byte fields when reading datasets
from HDF5 files.

For further information, see the following:

- https://docs.h5py.org/en/stable/strings.html
- https://github.com/h5py/h5py/issues/1769#issuecomment-770331250
- https://github.com/h5py/h5py/issues/1751
"""

import h5py
import polars as pl


def save_polars_to_hdf5(data_file, path, data_frame):
    np_arr = data_frame.to_numpy(structured=True)
    # Identify string fields, and replace their dtype with the special dtype
    # that h5py provides for HDF5 strings.
    dtype_out = []
    for name, (field_dtype, _field_offset) in np_arr.dtype.fields.items():
        if field_dtype.kind in ['S', 'U']:
            dtype_out.append((name, h5py.string_dtype()))
        else:
            dtype_out.append((name, field_dtype))
    # Convert all string fields in the structured array.
    np_out = np_arr.astype(dtype_out)
    # Save the resulting array as an HDF5 dataset.
    with h5py.File(data_file, 'w') as f:
        f[path] = np_out


def load_polars_from_hdf5(data_file, path):
    with h5py.File(data_file, 'r') as f:
        np_out = f[path][()]
    # Identify encoded byte fields with the special HDF5 string dtype.
    fields = np_out.dtype.fields
    decode_cols = [
        name
        for (name, (field_dtype, field_offset)) in fields.items()
        if h5py.check_string_dtype(field_dtype)
    ]
    # Decode each UTF-8 byte field.
    conversions = [pl.col(name).cast(pl.Utf8) for name in decode_cols]
    df = pl.DataFrame(np_out).with_columns(*conversions)
    return df


def test_polars_hdf5_string_fields(tmp_path):
    """
    Test that we can save a Polars dataframe with a string column to an HDF5
    file, and that we obtain an identical dataframe when reading from the same
    HDF5 file.
    """
    df = pl.DataFrame(
        {'x': [1, 2, 3], 'y': [4.0, 5.0, 6.0], 'z': ['a', 'b', 'c']}
    )
    data_file = tmp_path / 'polars_to_hdf5.hdf5'
    data_set = 'dataframe'

    save_polars_to_hdf5(data_file, data_set, df)

    df_back = load_polars_from_hdf5(data_file, data_set)
    assert df_back.equals(df)
