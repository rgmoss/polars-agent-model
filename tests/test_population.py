import numpy as np
import polars as pl
import polabm as abm
import pytest


def test_population_ages_over_time():
    popn_size = 10
    time_steps = 20
    column = 'age'

    popn = abm.Population(size=popn_size)
    popn.add_component(column, abm.Age())

    for _ in range(time_steps):
        popn.time_step()

    expected_ages = pl.Series(np.arange(time_steps, time_steps + popn_size))
    assert all(popn.df[column] == expected_ages)

    with pl.Config(
        tbl_formatting='ASCII_MARKDOWN',
        tbl_hide_column_data_types=True,
        tbl_hide_dataframe_shape=True,
    ):
        print(popn.df)


def test_population_ages_over_time_different_name():
    popn_size = 10
    time_steps = 20
    column = 'different_column'

    popn = abm.Population(size=popn_size)
    popn.add_component(column, abm.Age())

    for _ in range(time_steps):
        popn.time_step()

    expected_ages = pl.Series(np.arange(time_steps, time_steps + popn_size))
    assert 'age' not in popn.df.columns
    assert all(popn.df[column] == expected_ages)


def test_population_duplicate_component_names():
    popn_size = 10
    name = 'age'
    popn = abm.Population(size=popn_size)
    popn.add_component(name, abm.Age())

    with pytest.raises(ValueError):
        popn.add_component(name, abm.Age())
