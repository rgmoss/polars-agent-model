import logging
import polars as pl
import polabm as abm
import pytest


class EagerAddition:
    """Add a constant to each value in a column."""

    def __init__(self, column, increment):
        self._col = column
        self._incr = increment

    def apply_eager(self, popn):
        assert isinstance(popn, pl.DataFrame)
        return popn.with_columns(pl.col(self._col) + pl.lit(self._incr))


class LazySubtraction:
    """Subtract a constant from each value in a column."""

    def __init__(self, column, decrement):
        self._col = column
        self._decr = decrement

    def apply_lazy(self, popn):
        assert isinstance(popn, pl.LazyFrame)
        return popn.with_columns(pl.col(self._col) - pl.lit(self._decr))


class InvalidAction:
    """Define invalid actions that are neither eager or lazy."""

    def apply_invalid(self, popn):
        pass


def is_lazy(message):
    """Identify logging messages about creating lazy data frames."""
    return message.startswith('Creating lazy data frame')


def is_eager(message):
    """Identify logging messages about creating eager data frames."""
    return message.startswith('Collecting lazy data frame')


def conversions(records):
    """Return logging messages about data frame conversions."""
    return [r.msg for r in records if is_lazy(r.msg) or is_eager(r.msg)]


def test_actions_eager_and_lazy(caplog):
    """
    Test that applying a sequence of eager and lazy actions to a data frame
    produces the expected output.
    """
    initial_popn = pl.DataFrame({'x': [1, 2, 3, 4, 5], 'y': [10, 9, 8, 7, 6]})
    add_three_x = EagerAddition('x', 3)
    sub_two_y = LazySubtraction('y', 2)

    caplog.set_level(logging.DEBUG)

    actions = [sub_two_y, sub_two_y, add_three_x, add_three_x, sub_two_y]
    final_popn = abm.apply_actions(initial_popn, actions)
    assert isinstance(final_popn, pl.DataFrame)

    assert all(final_popn['x'] == initial_popn['x'] + 6)
    assert all(final_popn['y'] == initial_popn['y'] - 6)

    # Inspect the log messages to ensure there were no unnecessary conversions
    # between lazy and eager data frames.
    messages = conversions(caplog.records)
    num_lazy = len([m for m in messages if is_lazy(m)])
    num_collect = len([m for m in messages if is_eager(m)])
    assert num_lazy == 2
    assert num_collect == 2
    # NOTE: the lazy <-> eager conversions should alternate.
    assert all(is_lazy(m) for m in messages[::2])
    assert all(is_eager(m) for m in messages[1::2])


def test_actions_invalid_type(caplog):
    """
    Test that applying a sequence of eager, lazy, and invalid actions to a
    data frame raises an exception.
    """
    initial_popn = pl.DataFrame({'x': [1, 2, 3, 4, 5], 'y': [10, 9, 8, 7, 6]})
    add_three_x = EagerAddition('x', 3)
    sub_two_y = LazySubtraction('y', 2)
    invalid = InvalidAction()

    caplog.set_level(logging.DEBUG)

    with pytest.raises(TypeError):
        abm.apply_actions(initial_popn, [sub_two_y, add_three_x, invalid])

    # Inspect the log messages to ensure that the valid actions were applied.
    messages = conversions(caplog.records)
    num_lazy = len([m for m in messages if is_lazy(m)])
    num_collect = len([m for m in messages if is_eager(m)])
    assert num_lazy == 1
    assert num_collect == 1
