from collections.abc import Iterable
from typing import Protocol, runtime_checkable

import logging
import numpy as np
import polars as pl

__all__ = [
    'EagerAction',
    'LazyAction',
    'apply_actions',
    'Population',
    'Component',
    'Age',
]


@runtime_checkable
class EagerAction(Protocol):
    """
    Define a series of operations that will be executed immediately.
    """

    def apply_eager(self, df: pl.DataFrame) -> pl.DataFrame:
        """
        Apply one or more operations to the data frame.
        """


@runtime_checkable
class LazyAction(Protocol):
    """
    Define a series of lazy operations that will not be executed until their
    output is required.
    """

    def apply_lazy(self, df: pl.LazyFrame) -> pl.LazyFrame:
        """
        Apply one or more operations to the data frame.
        """


def apply_actions(
    popn: pl.DataFrame | pl.LazyFrame,
    actions: Iterable[EagerAction | LazyAction],
) -> pl.DataFrame:
    """
    Apply a series of actions to a data frame, and return the result.
    """
    logger = logging.getLogger(__name__)

    curr: pl.DataFrame | pl.LazyFrame = popn

    for action in actions:
        if isinstance(action, EagerAction):
            if isinstance(curr, pl.LazyFrame):
                logger.debug('Collecting lazy data frame for eager action')
                curr = curr.collect()
            curr = action.apply_eager(curr)
        elif isinstance(action, LazyAction):
            if isinstance(curr, pl.DataFrame):
                logger.debug('Creating lazy data frame for lazy action')
                curr = curr.lazy()
            curr = action.apply_lazy(curr)
        else:
            raise TypeError(f'Invalid action: {action}')

    if isinstance(curr, pl.LazyFrame):
        logger.debug('Collecting lazy data frame for return')
        curr = curr.collect()

    return curr


class Population:
    """
    Characterises a population as a collection of individuals, each of which
    is represented as a separate row with a unique ``id`` value.
    """

    def __init__(self, size: int, seed: int = 12345) -> None:
        """
        Create a new population of ``size`` individuals.
        """
        #: The data frame used to represent the current population state.
        self.df: 'pl.DataFrame' = pl.DataFrame({'id': np.arange(size)})
        #: Random number generator used for stochastic effects.
        self.rng: 'np.random.Generator' = np.random.default_rng(seed)
        #: The actions to perform at each time-step.
        self.actions: list[EagerAction | LazyAction] = []
        #: The actions to perform after each time-step.
        self.finalisers: list[EagerAction | LazyAction] = []
        #: The components that have been added to this population.
        self.components: 'dict[str, Component]' = {}
        #: The current time, counted as the number of time-steps that have
        #: already been performed.
        self.current_time: int = 0

    def add_component(self, name: str, comp: 'Component') -> None:
        """
        Add a component to this population.

        :raise ValueError: if a component of the same name already exists.
        """
        if name in self.components:
            raise ValueError(f'Component {name} already exists')
        self.components[name] = comp
        comp.register_population(self, name)

    def add_action(self, action: EagerAction | LazyAction) -> None:
        """
        Add an action to be performed at each time-step.

        .. important:: These actions should generally add new columns rather
           than modifying existing columns, and rely on finalisers to rename
           these new columns after all actions have been performed.
        """
        self.actions.append(action)

    def add_finaliser(self, action: EagerAction | LazyAction) -> None:
        """
        Add a finaliser action to be performed after each time-step.

        .. important:: Finalisers should generally only replace model columns
           with updated columns created by actions during the time-step.
           :class:`RenameFinaliser` should be sufficient for most cases.
        """
        self.finalisers.append(action)

    def time_step(self) -> None:
        """
        Perform a single time-step.
        """
        # Apply each action in turn, then apply each finaliser in turn.
        new_df = apply_actions(self.df, self.actions + self.finalisers)
        self.df = new_df
        self.current_time += 1


@runtime_checkable
class Component(Protocol):
    """
    A component implements a specific process within a population, such as
    ageing, mobility, infection, recovery, acquisition of immunity, and waning
    of immunity.
    """

    def register_population(self, popn: Population, name: str) -> None:
        """
        Add this component to the provided population.

        :param popn: The population to which the component will be added.
        :param name: The name of the component.
        """


class RenameFinaliser:
    """
    Rename temporary columns after all time-step actions have been performed.

    :param mapping: A dictionary that maps from old names (temporary columns)
        to new names (model state columns).
    :param drop_existing: Whether to remove the model state columns before
        renaming the temporary columns.
        Note that renaming will raise an exception if the new column already
        exists, so this should only be set to ``False`` when these columns are
        guaranteed not to exist.

    >>> import polars as pl
    >>> import polabm
    >>> age_finaliser = polabm.RenameFinaliser({'age_updated': 'age'})
    >>> df = pl.DataFrame({'age': [1, 2, 3, 4, 5]})
    >>> df_updated = df.with_columns(age_updated=pl.col('age') + 1)
    >>> df_new = polabm.apply_actions(df_updated, [age_finaliser])
    >>> with pl.Config(
    ...     tbl_formatting='ASCII_MARKDOWN',
    ...     tbl_hide_column_data_types=True,
    ...     tbl_hide_dataframe_shape=True,
    ... ):
    ...     print(df_updated)
    ...     print()
    ...     print(df_new)
    | age | age_updated |
    |-----|-------------|
    | 1   | 2           |
    | 2   | 3           |
    | 3   | 4           |
    | 4   | 5           |
    | 5   | 6           |
    <BLANKLINE>
    | age |
    |-----|
    | 2   |
    | 3   |
    | 4   |
    | 5   |
    | 6   |
    """

    def __init__(self, mapping: dict[str, str], drop_existing=True):
        self.mapping = mapping
        self.drop_existing = drop_existing

    def apply_lazy(self, popn: pl.LazyFrame) -> pl.LazyFrame:
        if self.drop_existing:
            popn = popn.drop(self.mapping.values())
        return popn.rename(self.mapping)


class Age:
    r"""
    Define the age of each agent, and increase age by 1 at each time-step.

    .. note::

       This class is both a :class:`Component` **and** a :class:`LazyAction`.
       The :meth:`register_population` method adds the ageing action
       implemented by :meth:`apply_lazy` to the population.

    .. warning::

       This assigns ages of :math:`\{0, \dots, N - 1\}` to the :math:`N`
       individuals in the population.
    """

    def register_population(self, popn: Population, name: str) -> None:
        """
        Initialise the population ages, and register the ageing action.

        This component will create a new column (``name``) to store ages.
        """
        self.column = name
        self.update = f'{name}_updated'
        initial_ages = np.arange(popn.df.height)
        popn.df = popn.df.with_columns(pl.lit(initial_ages).alias(name))
        popn.add_action(self)
        popn.add_finaliser(RenameFinaliser({self.update: self.column}))

    def apply_lazy(self, popn: pl.LazyFrame) -> pl.LazyFrame:
        """
        Increase the age of each agent by 1 at each time-step.
        """
        return popn.with_columns((pl.col(self.column) + 1).alias(self.update))
